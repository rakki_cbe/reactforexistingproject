package com.example.testwebview

import android.app.Application
import com.example.testwebview.reactnative.communication.CustomToastPackage
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.facebook.imagepipeline.core.ImageTranscoderType
import com.facebook.imagepipeline.core.MemoryChunkType
import com.facebook.react.PackageList
import com.facebook.react.ReactApplication
import com.facebook.react.ReactNativeHost
import com.facebook.react.ReactPackage


class ApplicationClass: Application(),ReactApplication{
    override fun onCreate() {
        super.onCreate();
        Fresco.initialize(this, ImagePipelineConfig.newBuilder(applicationContext)
            .setMemoryChunkType(MemoryChunkType.BUFFER_MEMORY)
            .setImageTranscoderType(ImageTranscoderType.JAVA_TRANSCODER)
            .experiment().setNativeCodeDisabled(true)
            .build())
        }
    /**
     * For react native
     */

    private val mReactNativeHost: ReactNativeHost? = object : ReactNativeHost(this) {
        override fun getUseDeveloperSupport(): Boolean {
            return BuildConfig.DEBUG
        }

        override fun getPackages(): List<ReactPackage> {
            // Packages that cannot be autolinked yet can be added manually here, for example:
            // packages.add(new MyReactNativePackage());
            val packages: MutableList<ReactPackage> =
                PackageList(this).packages
            // Packages that cannot be autolinked yet can be added manually here, for example:
            // packages.add(new MyReactNativePackage());
            packages.add(CustomToastPackage()) // <-- Add this line with your package name.

            return packages

            //https://reactnative.dev/docs/native-modules-android Doc available in this page
        }


        override fun getJSMainModuleName(): String {
            return "index"
        }
    }

    override fun getReactNativeHost(): ReactNativeHost? {
        return mReactNativeHost
    }
}