package com.example.testwebview


import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.testwebview.otp.poc.OTPScreen
import com.example.testwebview.reactnative.ReactNativeAcitivtyJava
import com.example.testwebview.reactnative.ReactNativeActivity
import kotlinx.android.synthetic.main.activity_introduction.*


class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_introduction)
        setSupportActionBar(toolbar)
        var spanText = SpannableString(getString(R.string.term_cond_dec))
        var string = getString(R.string.terms_cond) // String which need to highlighted in string
        var stringDesc= getString(R.string.term_cond_dec)  //to get index of highlight text

        val insdex_start = stringDesc.indexOf(string)
        val index_end= insdex_start +string.length
        spanText.setSpan(object :ClickableSpan(){
            override fun onClick(p0: View) {
                startActivity(Intent(applicationContext, MainActivity::class.java))
            }

        },insdex_start,index_end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanText.setSpan(UnderlineSpan(),insdex_start,index_end,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanText.setSpan(ForegroundColorSpan(Color.BLACK),insdex_start,index_end,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanText.setSpan(StyleSpan(Typeface.BOLD),insdex_start,index_end,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
       checkbox.text = spanText
        checkbox.setMovementMethod(LinkMovementMethod.getInstance());//With out this click of span wont work

        go_to_otp.setOnClickListener{
            //We can get the sh key by running this .. IT will change based on debug and release
            /*val appSignatureHelper = AppSignatureHelper(this)
            appSignatureHelper.appSignatures*/
            startActivity(Intent(applicationContext, OTPScreen::class.java))
        }

        react_page.setOnClickListener{
            startActivity(Intent(applicationContext, ReactNativeAcitivtyJava::class.java))
        }

        // image library with glide
        Glide.with(this).load("https://scx1.b-cdn.net/csz/news/800/2019/2-nature.jpg").into(image)

        //Fresco
        my_image_view.setImageURI("https://scx1.b-cdn.net/csz/news/800/2019/2-nature.jpg")

        //Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle("Cash for call")

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                val intent = Intent(
                    Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:$packageName")
                )
                startActivityForResult(intent, 5675)
            }
        }*/


    }


    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 5675) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(this)) {
                    // SYSTEM_ALERT_WINDOW permission not granted
                }
            }
        }
    }

}
