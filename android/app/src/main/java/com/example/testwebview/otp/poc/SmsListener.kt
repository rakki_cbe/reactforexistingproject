package com.example.testwebview.otp.poc

interface SmsListener {
    fun messageReceived(sender: String?, messageText: String?)
}