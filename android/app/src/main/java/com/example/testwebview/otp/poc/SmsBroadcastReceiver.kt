package com.example.testwebview.otp.poc

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status


class SmsBroadcastReceiver : BroadcastReceiver() {

    private var mSmsListener: SmsListener? = null


    override fun onReceive(context: Context, intent: Intent) {
        Log.e("BROADCAST RECEIVER", "onReceive called()")
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            val data = intent.extras
            val mStatus = data?.get(SmsRetriever.EXTRA_STATUS) as Status
            when (mStatus.getStatusCode()) {
                CommonStatusCodes.SUCCESS -> {
                    // Get SMS message contents'
                    val message = data.get(SmsRetriever.EXTRA_SMS_MESSAGE) as String
                    mSmsListener?.messageReceived("", message)
                    }
                CommonStatusCodes.TIMEOUT -> {
                    // Waiting for SMS timed out (5 minutes)
                    Log.d("Test", "onReceive: failure");
                }
            }
        }

            /*  val pdus = data!!["pdus"] as Array<Any>?
              for (i in pdus!!.indices) {
                  val smsMessage: SmsMessage = SmsMessage.createFromPdu(pdus[i] as ByteArray)
                  val sender: String = smsMessage.getDisplayOriginatingAddress()
                  //You must check here if the sender is your provider and not another one with same text.
                  val messageBody: String = smsMessage.getMessageBody()
                  //Pass on the text to our listener.
                  mSmsListener?.messageReceived(sender, messageBody)
              }*/
    }

    fun bindListener(listener: SmsListener?) {
        mSmsListener = listener
    }
}
