package com.example.testwebview.reactnative;

import android.os.Bundle;

import com.example.testwebview.reactnative.communication.CallBackExample;
import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;

public class ReactNativeAcitivtyJava extends ReactActivity {
    @Override
    protected String getMainComponentName() {
        return "DemoApp";
    }
    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
            protected Bundle getLaunchOptions() {
                Bundle initialProperties = new Bundle();
                initialProperties.putString("UserName","ReactUser");
                initialProperties.putString("Password","password");
                return initialProperties;
            }
        };
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if(CallBackExample.INSTANCE.getCallBack()!=null && hasFocus)
            CallBackExample.INSTANCE.getCallBack().invoke();
    }
}
