package com.example.testwebview

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        CFCTermsConditions.loadUrl("https://developer.android.com/kotlin/get-started#declare_an_activity")
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.declare_an_activity -> CFCTermsConditions.loadUrl("https://developer.android.com/kotlin/get-started#declare_an_activity")
            R.id.create_an_item_click_listener -> CFCTermsConditions.loadUrl("https://developer.android.com/kotlin/get-started#create_an_item_click_listener")
            R.id.add_kotlin_into_an_existing_app -> CFCTermsConditions.loadUrl("https://developer.android.com/kotlin/get-started#add_kotlin_into_an_existing_app")
            R.id.create_an_on_click_listener -> CFCTermsConditions.loadUrl("https://developer.android.com/kotlin/get-started#create_an_on-click_listener")
            R.id.error_click_listener -> CFCTermsConditions.loadUrl("https://developer.android.com/kotlin/get-started#error")
            else -> super.onOptionsItemSelected(item)
        }
        return  true
    }
}
