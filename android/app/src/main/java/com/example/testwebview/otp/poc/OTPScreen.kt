package com.example.testwebview.otp.poc

import android.Manifest
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.testwebview.MainActivity
import com.example.testwebview.R
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import kotlinx.android.synthetic.main.activity_otpscreen.*
import java.util.regex.Pattern


class OTPScreen : AppCompatActivity() {

    private val PERMISSION_REQUEST_ID = 100
    private val TAG = MainActivity::class.java.simpleName
    var mSmsBroadcastReceiver: SmsBroadcastReceiver? = null
    private val BROADCAST_ACTION = "android.provider.Telephony.SMS_RECEIVED"
    private var intentFilter: IntentFilter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otpscreen)
        mSmsBroadcastReceiver = SmsBroadcastReceiver()
        intentFilter = IntentFilter()
        intentFilter?.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
        startSmsUserConsent()
        setListener()
    }

    private fun setListener() {
        mSmsBroadcastReceiver?.bindListener(object : SmsListener {
            override fun messageReceived(
                sender: String?,
                messageText: String?
            ) {
                val pattern = Pattern.compile("\\d{6}")
                val matcher = pattern.matcher(messageText)
                if (matcher.find()) {
//                                  Update UI
                    otp.setText( matcher.group(0))
                    Toast.makeText(applicationContext, "Your OTP is :" + matcher.group(0), Toast.LENGTH_SHORT).show()
                }
                Log.e("Text", messageText)
                Toast.makeText(
                    this@OTPScreen,
                    "Message: $messageText",
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    private fun requestRuntimePermissions(vararg permissions: String) {
        for (perm in permissions) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    perm
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(perm),
                    PERMISSION_REQUEST_ID
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_ID) {
            if (grantResults.size > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {

                // permission was granted
            } else {
                Log.e(TAG, "Permission not granted")
            }
        }
    }
    private fun startSmsUserConsent() {
        val client = SmsRetriever.getClient(this);
        //We can add sender phone number or leave it blank
        // I'm adding null here
        client.startSmsRetriever().addOnSuccessListener({
                Toast.makeText(getApplicationContext(), "On Success", Toast.LENGTH_LONG).show();
        }).addOnFailureListener( {
                Toast.makeText(getApplicationContext(), "On OnFailure", Toast.LENGTH_LONG).show();
            }
        );
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(mSmsBroadcastReceiver, intentFilter)
        Log.e("MainActivity", "Registered receiver")
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(mSmsBroadcastReceiver)
        Log.e("MainActivity", "Unregistered receiver")
    }
}
