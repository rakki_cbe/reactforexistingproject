package com.example.testwebview.reactnative.communication

import com.facebook.react.bridge.Callback
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod

object CallBackExample : ReactContextBaseJavaModule() {
    var callBack:Callback?=null
    override fun getName(): String {
        return "CallBackExample"
    }

    @ReactMethod
    fun checkCallBack(callBack:Callback){
        this.callBack = callBack
    }
}