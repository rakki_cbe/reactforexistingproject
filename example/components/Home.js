import * as React from 'react';
import { Text, View } from 'react-native';
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import Login from './login/Login'
import Sucess from './login/Sucess'
import Network from './Users/ShowUsers'
import CreateUser from './login/RegisterUser'
/**
 * ToastExample is call back triggerd from JS to native 
 * CallBackExample is call back triggered from native to JS 
 */
import {ToastExample,CallBackExample} from '../utils';


const Stack = createStackNavigator() 
/**
 * 
 * @param {*} props  This passed from activity 
 */
function Home(props) {
  
  CallBackExample.checkCallBack(()=>{
    ToastExample.show("Window focus changes", ToastExample.SHORT)
  })
  //alert(JSON.stringify(props))
  return (
    <NavigationContainer>
    <Stack.Navigator>
        <Stack.Screen
        name="Login"
        component={Login}
        initialParams={props}
        options={{title: 'Welcome'}}
        />
         <Stack.Screen
        name="Sucess"
        component={Sucess}
        options={{title: 'Sucess'}}
        />
         <Stack.Screen
        name="Network"
        component={Network}
        options={{title: 'Network'}}
        />
        <Stack.Screen
        name="Create"
        component={CreateUser}
        options={{title: 'Create User'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


export default Home;

