import React,{Component} from 'react'
import {StyleSheet,TouchableOpacity,Text,View} from 'react-native'

class stateExample extends Component{
    //We are initializing state value for component 
    state ={
        count : 0
    }

    onPress = () =>{
        //Asigning new value to our state 
        this.setState({
            count: this.state.count+1
        })
    }

    render(){
        return(
        <View style={styles.container}>
        <TouchableOpacity
         style={styles.button}
         //Creating bridge between our function and touch event 
         onPress={this.onPress}
        >
         <Text>Click me</Text>
        </TouchableOpacity>
        <View style={styles.countContainer}>
          <Text>
            You clicked { this.state.count } times
          </Text>
        </View>
      </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    button: {
      alignItems: 'center',
      backgroundColor: '#DDDDDD',
      padding: 10,
      marginBottom: 10
    }
  })

  export default stateExample