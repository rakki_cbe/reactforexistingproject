import React from 'react'
import {Text,View,StyleSheet} from 'react-native'

const styleNew = StyleSheet.create({
    center:{
        alignItems:'center'
    }
})

function Greeting(props){
    return(
        <View style={styleNew.center}>
            <Text>Hello {props.name}</Text>
        </View>
    )
}

function LoatsOfGreeting(){
    return(
        <View style={[styleNew.center,{top:50}]}>
            <Greeting name="Android"/>
            <Greeting name="IOS"/>
            <Greeting name="Web"/>
        </View>
    )
}
export default LoatsOfGreeting