import * as React from 'react'
import {Text,TextInput,Button,View,ScrollView} from 'react-native'
import {loginStyle} from './LoginStyle'
import {isFourmValid} from './LoginFunction'
import {creatUser} from '../network/UserDao'

export default function Login({ navigation }){
    const[name,setName] = React.useState("");
    const[userName,setUserName] = React.useState("");
    const[password,setPassword] = React.useState("");
    const[isValid,setValid]=React.useState(false)
    const [isLoading, setLoading] = React.useState(true);
    const [sucessCode, setLoginSucessCode] = React.useState("");
    if (sucessCode=="1111") {
        navigation.navigate("Login")
        setLoginSucessCode("")
    } else if(sucessCode=="0000") {
        navigation.navigate("Sucess",{sucess:false})
        setLoginSucessCode("")
    }
    return(
        <ScrollView style={loginStyle.scrollView}>
            <View style ={loginStyle.container}>
                <Text style={loginStyle.title}>Create User</Text>
                <View style={loginStyle.inputTextContainer}>
                    <Text style={loginStyle.inputTitle}>Name :</Text>
                        <TextInput style={loginStyle.inputText}
                            placeholder =""
                            onChangeText={text => {
                                setName(text)
                                setValid(isFourmValid(userName,password))
                            }
                        }
                            defaultValue = {name}/>
                    </View>
                    <View style={loginStyle.inputTextContainer}>
                    <Text style={loginStyle.inputTitle}>UserName :</Text>
                        <TextInput style={loginStyle.inputText}
                            placeholder =""
                            onChangeText={text => {
                                setUserName(text)
                                setValid(isFourmValid(userName,password))
                            }
                        }
                            defaultValue = {userName}/>
                    </View>
                    <View style={loginStyle.inputTextContainer}>
                    <Text style={loginStyle.inputTitle}>Password :</Text>
                    <TextInput style={loginStyle.inputText}
                        placeholder =""
                        onChangeText={text => {
                            setPassword(text)
                            setValid(isFourmValid(userName,password))
                        }
                        }
                        secureTextEntry = {true}
                        defaultValue = {password}/>
                    </View>
                    <View style={loginStyle.buttonContainer}>
                        <Button style={{width: 150, height: 50}} 
                        title="Clear" onPress ={()=> {
                            setUserName("");
                            setPassword("");
                            setName("")
                        }} />
                        <Button style={{width: 150,height: 50}}
                        onPress ={()=>{
                            creatUser({'name':name,'userName':userName,'password':password},setLoading,setLoginSucessCode)
                    
                        }
                    }
                    disabled= {!isValid}
                        title="Submit"/>
                    </View>
                    <Button title="Login" 
                    onPress={()=> navigation.navigate("Login")}/>
                    
            </View>
        </ScrollView>
    );
}