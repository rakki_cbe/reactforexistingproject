import React from 'react';
import { StyleSheet } from 'react-native';

const loginStyle = StyleSheet.create({
    scrollView:{
      backgroundColor :"grey"
    },
  container: {
      marginTop: 50,
      width:250, 
      alignSelf:"center"
    },
    buttonContainer: {
        padding:10, 
        alignItems:"stretch", 
        flexDirection:"row", 
        flex: 1,
        justifyContent:"space-between"},
        
 title:{
  color: 'black',
  fontSize: 24,
  flex:1,
  marginTop: 8,
  alignSelf:"center"
 },
 inputTitle:{
  color: 'black',
  fontSize: 14,
  marginStart: 8
},
    inputText: {
      color: 'black',
      fontSize: 14,
      marginStart: 8
    },
    inputTextContainer: {
      flex:1,
      flexDirection:"row",
      alignItems: "center"
}
  });

  export {loginStyle}
  
