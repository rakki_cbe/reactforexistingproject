/**
 * @format
 */
import {AppRegistry} from 'react-native';
import Home from './example/components/Home'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Home);
