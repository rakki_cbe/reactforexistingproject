## React for existing android application documents ##

---
## Android manifest ##

<application
…..
android:usesCleartextTraffic="true" tools:targetApi="28"
>

<activity
    android:name=“.YourActivity name ”
    android:label="@string/app_name"
    android:theme="@style/Theme.AppCompat.Light.NoActionBar"> <!— We need to use this theme
  </activity>

---

## Build in root ##

allprojects {
    repositories {
     …..
        maven {
            // All of React Native (JS, Obj-C sources, Android binaries) is installed from npm
            url("$rootDir/../node_modules/react-native/android")
        }
        maven {
            // Android JSC is installed from npm
            url("$rootDir/../node_modules/jsc-android/dist")
        }

        google()
        jcenter()
        maven { url 'https://www.jitpack.io' }
    }
---

## Build app ##
/** Resolve the error of Hermes not available **/
project.ext.react = [
        enableHermes: false,  // clean and rebuild if changing
]
def jscFlavor = 'org.webkit:android-jsc:+'
def enableHermes = project.ext.react.get("enableHermes", false);


dependencies {
…..
//React native
implementation "com.facebook.react:react-native:+"
if (enableHermes) {
    def hermesPath = "../../node_modules/hermes-engine/android/";
    debugImplementation files(hermesPath + "hermes-debug.aar")
    releaseImplementation files(hermesPath + "hermes-release.aar")
} else {
    implementation jscFlavor
}
---

** Just follow the document to create react activity **

 https://reactnative.dev/docs/integration-with-existing-apps


** Note : If you have changed port when u start your react native server then **

    1. mReactRootView?.startReactApplication(mReactInstanceManager, "MyReactNativeApp", null)
    2. Delete this line from you activity
    3. Run you app -> go to activity
    4. Press cmd+m -> settings ->Dev port setting
    5. Enter 10.0.2.2:8088(port number given when u start react-native)
    6. Once you set port successfully
    7. Add this back mReactRootView?.startReactApplication(mReactInstanceManager, "MyReactNativeApp", null)

---
### Who do I talk to? ###

* Radhakrishnan (rakki.bsc2009@gmail.com)
* Freelancer